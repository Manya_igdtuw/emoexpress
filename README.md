# Mind Matrix
This project implements a Sudoku solver with a graphical user interface (GUI) using Haskell. The solver is based on Peter Norvig's algorithm, which employs constraint propagation and search techniques to solve Sudoku puzzles.

## Features and using the GUI
Graphical Interface: Allows users to input Sudoku puzzles via a 9x9 grid.
Solve Button: Initiates the solving process using Peter Norvig's algorithm.
Clear Button: Resets the grid for entering new Sudoku puzzles.


## Algorithm
The solver uses Peter Norvig's approach to solve Sudoku puzzles:

Constraint Propagation: Elimination and only-choice strategies to reduce possibilities.
Search: Backtracking search to find the solution.

## Getting Started
To access and run the project locally, follow these steps:

## Prerequisites
Make sure to have the following installed:

Node.js (version 12 or higher)
npm (Node Package Manager)

## Installation
Clone the repository to the local machine:

git clone https://gitlab.com/Manya_igdtuw/mind_matrix.git

cd mind_matrix

## Install dependencies:
npm install

## Running the Application
To start the application locally:

"npm start" in mind_matrix/mind_matrix_frontend
This will run the frontend of the application application in development mode. Open http://localhost:3000 in your browser to view it.
 
"node server.js" in mind_matrix/mind_matrix_backend/backend 
This will run the backend server of the application successfully. Open http://localhost:3001 in your browser to view it.

## Refrences 
1. Peter Norvig Sudoku solver - https://norvig.com/sudoku.html
2. Haskell cafe Sudoku Solver - https://mail.haskell.org/pipermail/haskell-cafe/2007-August/031049.html



