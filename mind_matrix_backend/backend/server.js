const express = require('express');
const bodyParser = require('body-parser');
const { exec } = require('child_process');
const cors = require('cors');

const app = express();
const port = 3001;

app.use(bodyParser.json());
app.use(cors());

app.post('/solve', (req, res) => {
    console.log("POST /solve request received");
    const grid = req.body.grid.join('');
    console.log("Received grid:", grid);

    exec(`runhaskell solveSudoku.hs ${grid}`, (error, stdout, stderr) => {
        if (error) {
            console.error(`Error executing Haskell script: ${error.message}`);
            return res.status(500).send('Error solving Sudoku');
        }

        if (stderr) {
            console.error(`Haskell script error output: ${stderr}`);
            return res.status(500).send('Error solving Sudoku');
        }

        const solution = stdout.trim(); // Trim any extra whitespace
        console.log("Solution:", solution);
        res.json({ solution }); // Send solution as a single string
    });
});

app.listen(port, () => {
    console.log(`Sudoku solver backend running on port ${port}`);
});
