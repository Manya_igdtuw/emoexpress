import React, { useState } from 'react';
import SudokuGrid from './SudokuGrid';
import SudokuControls from './SudokuControls';
import './MainContent.css';

const MainContent = () => {
    const initialGrid = Array(81).fill('');
    const [grid, setGrid] = useState(initialGrid);
    

    const handleClearGrid = () => {
        setGrid(initialGrid.slice()); 
    };

    
/*
    const handleCheck = () => {
        console.log("Current grid:", grid); 
        const result = isValidSudoku(grid);
        if (result === 'incomplete') {
            alert("The Sudoku grid is not filled completely. Unable to check.");
        } else if (result === 'invalid') {
            alert("There are errors in the Sudoku solution.");
        } else {
            alert("Congratulations! The Sudoku is correctly solved.");
        }
    };
    */

    const handleSolve = async () => {
        try {
            const response = await fetch('http://localhost:3001/solve/', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ grid: grid.map(val => val || '0') }),
            });
    
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
    
            const data = await response.json();
            console.log("Response from solve endpoint:", data);
    
            
            if (typeof data.solution !== 'string') {
                throw new Error('Invalid solution format received');
            }
    
            // Remove all non-digit characters from the solution string
            const solutionDigits = data.solution.replace(/[^\d]/g, '');
    
            
            if (solutionDigits.length !== 81) {
                throw new Error('Invalid solution length received');
            }
    
            
            const newGrid = solutionDigits.split('').map(num => num === '0' ? '' : num);
            setGrid(newGrid);
        } catch (error) {
            console.error('Error solving Sudoku:', error);
        }
    };
    
    
    
    

    const handleChange = (row, col, value) => {
        const index = row * 9 + col;
        const newGrid = [...grid];
        newGrid[index] = value;
        setGrid(newGrid);
    };

    return (
        <main>
            <SudokuGrid grid={grid} onChange={handleChange} />
            <SudokuControls 
                onClear={handleClearGrid} 
                // onCheck={handleCheck} 
                onSolve={handleSolve} 
            />
        </main>
    );
};


const isValidSudoku = (grid) => {
    
    for (let i = 0; i < grid.length; i++) {
        if (grid[i] === '') {
            return 'incomplete'; 
        }
    }

    
    for (let row = 0; row < 9; row++) {
        const rowSet = new Set();
        for (let col = 0; col < 9; col++) {
            const value = grid[row * 9 + col];
            if (value !== '' && rowSet.has(value)) {
                return 'invalid';
            }
            rowSet.add(value);
        }
    }

    // Check columns
    for (let col = 0; col < 9; col++) {
        const colSet = new Set();
        for (let row = 0; row < 9; row++) {
            const value = grid[row * 9 + col];
            if (value !== '' && colSet.has(value)) {
                return 'invalid';
            }
            colSet.add(value);
        }
    }

    
    for (let boxRow = 0; boxRow < 3; boxRow++) {
        for (let boxCol = 0; boxCol < 3; boxCol++) {
            const boxSet = new Set();
            for (let row = 0; row < 3; row++) {
                for (let col = 0; col < 3; col++) {
                    const value = grid[(boxRow * 3 + row) * 9 + (boxCol * 3 + col)];
                    if (value !== '' && boxSet.has(value)) {
                        return 'invalid';
                    }
                    boxSet.add(value);
                }
            }
        }
    }

    return 'valid';
};

export default MainContent;
