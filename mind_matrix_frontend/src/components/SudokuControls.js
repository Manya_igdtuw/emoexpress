import React from 'react';
import './SudokuControls.css';
import '@fortawesome/fontawesome-free/css/all.min.css'; // Import Font Awesome CSS

const SudokuControls = ({ onClear, onCheck, onSolve }) => {
    return (
        <div className="button-container">
            <button className="sudoku-button" onClick={onClear}>
                <i className="fas fa-eraser"></i> Clear Grid
            </button>
            
            <button className="sudoku-button" onClick={onSolve}>
                <i className="fas fa-magic"></i> Solve
            </button>
        </div>
    );
};

export default SudokuControls;