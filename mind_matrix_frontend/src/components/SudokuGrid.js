import React from 'react';
import './SudokuGrid.css';

const SudokuGrid = ({ grid, onChange }) => {
    if (!Array.isArray(grid)) {
        console.error('Invalid grid passed to SudokuGrid:', grid);
        return null; 
    }
    const handleChange = (e, index) => {
        const value = e.target.value;
        if (/^[1-9]?$/.test(value)) { 
            onChange(Math.floor(index / 9), index % 9, value);
        }
    };

    const renderCells = () => {
        return grid.map((value, index) => (
            <input
                key={index}
                className="sudoku-cell"
                type="text"
                value={value}
                maxLength="1"
                onChange={(e) => handleChange(e, index)}
            />
        ));
    };

    return (
        <div className="sudoku-page">
            <div className="sudoku-container">
                <div className="sudoku-grid">
                    {renderCells()}
                </div>
            </div>
        </div>
    );
};

export default SudokuGrid;
