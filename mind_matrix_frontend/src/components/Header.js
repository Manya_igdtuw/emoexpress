import React from 'react';
import './Header.css';
import { FaCalendarAlt } from 'react-icons/fa'; 

const Header = () => {
    return (
        <header className="header">
        
            <div className="header-content">
                <div className="header-title">
                    <h1>Sudoku : Mind Matrix</h1>
                </div>
                <div className="header-creators">
                    <p>Created by: Manya Singh & Sakshi Soni</p>
                </div>
            </div>
        </header>
    );
};

export default Header;
