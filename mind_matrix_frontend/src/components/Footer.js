import React from 'react';
import './Footer.css';

const Footer = () => {
    return (
        <footer className="footer">
            <span className="left">@created by Manya Singh & Sakshi Soni</span>
            <span className="right">WE C6</span>
        </footer>
    );
};

export default Footer;